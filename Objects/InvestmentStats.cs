namespace budget_calculator.Objects
{
    public class InvestmentStats
    {
        public decimal YearPerDayAmount { get; set; }

        public decimal YearInvestmentTotal { get; set; }
    }
}