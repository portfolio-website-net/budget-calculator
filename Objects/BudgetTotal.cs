namespace budget_calculator.Objects
{
    public class BudgetTotal
    {
        public string Type { get; set; }

        public string Name { get; set; }

        public decimal Amount { get; set; }

        public bool IsPercentage { get; set; }
    }
}