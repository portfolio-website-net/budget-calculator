namespace budget_calculator.Objects
{
    public class NewCategoryParameters
    {
        public string Type { get; set; }

        public string Name { get; set; }

        public string InterestRate { get; set; }

        public string Amount { get; set; }
    }
}