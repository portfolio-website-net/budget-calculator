using System;
using Newtonsoft.Json;

namespace budget_calculator.Objects
{
    [Serializable]
    public class BudgetValue
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonIgnore]
        public string InterestRateStr { get; set; }

        [JsonProperty("interestRate")]
        public decimal? InterestRate 
        { 
            get
            {
                if (!string.IsNullOrEmpty(InterestRateStr))
                {
                    return decimal.TryParse(InterestRateStr.Replace(",", string.Empty), out decimal interestRateVal) ? interestRateVal : 0;
                }
                else
                {
                    return 0;
                }
            }

            set
            {
                InterestRateStr = value != null ? value.ToString() : null;
            }   
        }

        [JsonIgnore]
        public decimal InterestRateVal
        {
            get 
            {
                return InterestRate ?? 0;
            }
        }

        [JsonIgnore]
        public string AmountStr { get; set; }

        [JsonProperty("amount")]
        public decimal Amount
        { 
            get
            {
                if (!string.IsNullOrEmpty(AmountStr))
                {
                    return decimal.TryParse(AmountStr.Replace(",", string.Empty), out decimal amountVal) ? amountVal : 0;
                }
                else
                {
                    return 0;
                }
            }    

            set
            {
                AmountStr = value.ToString();
            }        
        }

        // Helper fields
        [JsonIgnore]
        public decimal TotalInvestmentAmount { get; set; }

        [JsonIgnore]
        public decimal TotalPreTaxInvestmentAmount { get; set; }
    }
}