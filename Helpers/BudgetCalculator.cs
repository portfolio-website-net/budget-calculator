using System.Linq;
using System.Collections.Generic;
using budget_calculator.Objects;

namespace budget_calculator.Helpers
{
    public class BudgetCalculator
    {
        private int[] yearIntervals = new int[] {1, 5, 10, 15, 20};
        private decimal monthlyExpensesTotal;
        private decimal monthlyIncomeTotal;
        private decimal initialInvestmentTotal;
        private decimal yearlySavings;
        private List<InvestmentStats> investmentStats;
        private decimal initialPreTaxInvestmentTotal;
        private List<InvestmentStats> preTaxInvestmentStats;
        private decimal preTaxInvestmentContributionTotal;

        public List<BudgetValue> GetDefaultBudgetValues()
        {
            return new List<BudgetValue>
            {
                new BudgetValue { Type = "Expense", Name = "Rent", Amount = 1500 },
                new BudgetValue { Type = "Expense", Name = "Electricity", Amount = 75 },
                new BudgetValue { Type = "Expense", Name = "Internet", Amount = 80 },
                new BudgetValue { Type = "Expense", Name = "Phone", Amount = 100 },
                new BudgetValue { Type = "Expense", Name = "Groceries", Amount = 400 },
                new BudgetValue { Type = "Expense", Name = "Insurance", Amount = 120 },
                new BudgetValue { Type = "Expense", Name = "Clothing", Amount = 100 },
                new BudgetValue { Type = "Expense", Name = "Entertainment", Amount = 500 },
                new BudgetValue { Type = "Expense", Name = "Vacation", Amount = 2000 },
                new BudgetValue { Type = "Expense", Name = "Fuel", Amount = 100 },
                new BudgetValue { Type = "Expense", Name = "Vehicle Maintenance", Amount = 100 },
                new BudgetValue { Type = "Expense", Name = "Fitness", Amount = 50 },
                new BudgetValue { Type = "Expense", Name = "Haircut", Amount = 15 },

                new BudgetValue { Type = "Income", Name = "Paycheck", Amount = 8000 },

                new BudgetValue { Type = "Investment", Name = "1 Year CD", InterestRate = 3, Amount = 180000 },
                new BudgetValue { Type = "Investment", Name = "Savings", InterestRate = 2, Amount = 15000 },

                new BudgetValue { Type = "PreTaxInvestment", Name = "401k", InterestRate = 6, Amount = 400000 },
                new BudgetValue { Type = "PreTaxInvestment", Name = "Traditional IRA", InterestRate = 5, Amount = 100000 },
                new BudgetValue { Type = "PreTaxInvestment", Name = "HSA", InterestRate = 2, Amount = 15000 },

                new BudgetValue { Type = "PreTaxInvestmentContribution", Name = "401k", Amount = 2000 },
                new BudgetValue { Type = "PreTaxInvestmentContribution", Name = "Traditional IRA", Amount = 250 },
                new BudgetValue { Type = "PreTaxInvestmentContribution", Name = "HSA", Amount = 200 },
            };
        }

        public void UpdateCommonOverallTotals(List<BudgetValue> budgetValues)
        {
            monthlyExpensesTotal = GetTypeList("Expense", budgetValues).Sum(x => x.Amount);
            monthlyIncomeTotal = GetTypeList("Income", budgetValues).Sum(x => x.Amount);
            initialInvestmentTotal = GetTypeList("Investment", budgetValues).Sum(x => x.Amount);            
            yearlySavings = (monthlyIncomeTotal - monthlyExpensesTotal) * 12;
            investmentStats = GetInvestmentStats(budgetValues, initialInvestmentTotal);
            initialPreTaxInvestmentTotal = GetTypeList("PreTaxInvestment", budgetValues).Sum(x => x.Amount);
            preTaxInvestmentStats = GetPreTaxInvestmentStats(budgetValues, initialPreTaxInvestmentTotal);
            preTaxInvestmentContributionTotal = GetTypeList("PreTaxInvestmentContribution", budgetValues).Sum(x => x.Amount);
        }

        public List<BudgetValue> GetTypeList(string type, List<BudgetValue> budgetValues)
        {
            return budgetValues.Where(x => x.Type == type).ToList();
        }

        public List<BudgetTotal> GetTypeTotalsList(string type, List<BudgetValue> budgetValues)
        {
            var result = new List<BudgetTotal>();            
            
            if (type == "Expense")
            {
                result.Add(new BudgetTotal
                    { 
                        Type = "Expense", 
                        Name = "Monthly Expenses Total", // Total of all monthly expense line items
                        Amount = monthlyExpensesTotal 
                    }
                );

                foreach (var yearInterval in yearIntervals)
                {
                    result.Add(new BudgetTotal
                        { 
                            Type = "Expense", 
                            Name = $"{yearInterval} Year Expenses", // [X] Year Expenses
                            Amount = monthlyExpensesTotal * 12 * yearInterval // Monthly Expenses Total x 12 x [X]
                        }
                    );
                }
            }
            else if (type == "Income") 
            {
                result.Add(new BudgetTotal
                    { 
                        Type = "Income", 
                        Name = "Monthly Income Total", // Total of all monthly income line items
                        Amount = monthlyIncomeTotal
                    }
                );

                foreach (var yearInterval in yearIntervals)
                {
                    result.Add(new BudgetTotal
                        { 
                            Type = "Income", 
                            Name = $"{yearInterval} Year Income", // [X] Year Income
                            Amount = monthlyIncomeTotal * 12 * yearInterval // Monthly Income Total x 12 x [X]
                        }
                    );
                }
            }
            else if (type == "Investment") 
            {
                result.Add(new BudgetTotal
                    { 
                        Type = "Investment", 
                        Name = "Initial Investment Total", // Total of all investment line items
                        Amount = initialInvestmentTotal 
                    }
                );

                var year = 1;
                while (year <= 20) 
                {
                    if (year == 1 || year == 5 || year == 10 || year == 15 || year == 20) 
                    {
                        var investmentStat = investmentStats[year - 1];

                        result.Add(new BudgetTotal
                            { 
                                Type = "Investment", 
                                Name = $"{year} Year - Earned Per Day", 
                                Amount = investmentStat.YearPerDayAmount 
                            }
                        );

                        result.Add(new BudgetTotal
                            { 
                                Type = "Investment", 
                                Name = $"{year} Year Investment Total", 
                                Amount = investmentStat.YearInvestmentTotal 
                            }
                        );
                    }

                    year++;
                }                    
            }
            else if (type == "PreTaxInvestment") 
            {
                result.Add(new BudgetTotal
                    { 
                        Type = "PreTaxInvestment", 
                        Name = "Initial Pre-Tax Investment Total", // Total of all pre-tax investment line items
                        Amount = initialPreTaxInvestmentTotal
                    }
                );
                
                var year = 1;
                while (year <= 20) 
                {
                    if (year == 1 || year == 5 || year == 10 || year == 15 || year == 20) 
                    {
                        var preTaxinvestmentStat = preTaxInvestmentStats[year - 1];

                        result.Add(new BudgetTotal
                            { 
                                Type = "PreTaxInvestment", 
                                Name = $"{year} Year - Earned Per Day", 
                                Amount = preTaxinvestmentStat.YearPerDayAmount
                            }
                        );

                        result.Add(new BudgetTotal
                            { 
                                Type = "PreTaxInvestment", 
                                Name = $"{year} Year Pre-Tax Investment Total", 
                                Amount = preTaxinvestmentStat.YearInvestmentTotal
                            }
                        );
                    }

                    year++;
                }
            }
            else if (type == "PreTaxInvestmentContribution") 
            {
                result.Add(new BudgetTotal
                    { 
                        Type = "PreTaxInvestment", 
                        Name = "Monthly Pre-Tax Contribution Total", // Total of all monthly pre-tax investment contribution line items
                        Amount = preTaxInvestmentContributionTotal
                    }
                );
                
                foreach (var yearInterval in yearIntervals)
                {
                    result.Add(new BudgetTotal
                        { 
                            Type = "PreTaxInvestment", 
                            Name = $"{yearInterval} Year Pre-Tax Contribution (No Interest)",
                            Amount = preTaxInvestmentContributionTotal * 12 * yearInterval // Monthly Pre-Tax Contribution Total x 12 x [X]
                        }
                    );
                }
            }
            else if (type == "Savings") 
            {
                result.Add(new BudgetTotal
                    { 
                        Type = "Savings", 
                        Name = "Hourly Savings", 
                        Amount = (yearlySavings / 365) / 24 // (Total Yearly Savings / 365) / 24
                    }
                );

                result.Add(new BudgetTotal
                    { 
                        Type = "Savings", 
                        Name = "Daily Savings", 
                        Amount = yearlySavings / 365 // Total Yearly Savings / 365
                    }
                );

                result.Add(new BudgetTotal
                    { 
                        Type = "Savings", 
                        Name = "Monthly Savings", 
                        Amount = monthlyIncomeTotal - monthlyExpensesTotal // Monthly Income Total – Monthly Expenses Total
                    }
                );

                foreach (var yearInterval in yearIntervals)
                {
                    result.Add(new BudgetTotal
                        { 
                            Type = "Savings", 
                            Name = $"{yearInterval} Year Savings", // [X] Year Savings
                            Amount = yearlySavings * yearInterval // Total Yearly Savings x [X]
                        }
                    );
                }

                result.Add(new BudgetTotal
                    { 
                        Type = "Savings", 
                        Name = "% Savings from Monthly Income", 
                        Amount = (monthlyIncomeTotal == 0 ? 0 : ((monthlyIncomeTotal - monthlyExpensesTotal) / monthlyIncomeTotal) * 100),
                            // ((Monthly Income Total – Monthly Expenses Total) / Monthly Income Total) x 100
                        IsPercentage = true
                    }
                );

                result.Add(new BudgetTotal
                    { 
                        Type = "Savings", 
                        Name = "Monthly Savings w/ Pre-Tax (No Interest)", 
                        Amount = (monthlyIncomeTotal - monthlyExpensesTotal) 
                            + preTaxInvestmentContributionTotal 
                            // (Monthly Income Total – Monthly Expenses Total) + Monthly Pre-Tax Contribution Total
                    }
                );
                                
                foreach (var yearInterval in yearIntervals)
                {
                    result.Add(new BudgetTotal
                        { 
                            Type = "Savings", 
                            Name = $"{yearInterval} Year Savings w/ Pre-Tax (No Interest)",
                            Amount = ((monthlyIncomeTotal - monthlyExpensesTotal)
                                + preTaxInvestmentContributionTotal) * 12 * yearInterval 
                                // ((Monthly Income Total – Monthly Expenses Total) + Monthly Pre-Tax Contribution Total) x 12 x [X]
                        }
                    );
                }

                foreach (var yearInterval in yearIntervals)
                {
                    result.Add(new BudgetTotal
                        { 
                            Type = "Savings", 
                            Name = $"{yearInterval} Year Savings w/ Pre-Tax w/ Interest - Earned Per Day",
                            Amount = investmentStats[yearInterval - 1].YearPerDayAmount
                                + preTaxInvestmentStats[yearInterval - 1].YearPerDayAmount
                                // [[X] Year Investments Earned Per Day Total] + [[X] Year Pre-Tax Investments Earned Per Day Total]
                        }
                    );

                    result.Add(new BudgetTotal
                        { 
                            Type = "Savings", 
                            Name = $"{yearInterval} Year Savings w/ Pre-Tax w/ Interest",
                            Amount = investmentStats[yearInterval - 1].YearInvestmentTotal
                                + preTaxInvestmentStats[yearInterval - 1].YearInvestmentTotal
                                // [[X] Year Investment Total] + [[X] Year Pre-Tax Investment Total]
                        }
                    );
                }
            }

            return result;
        }

        private List<InvestmentStats> GetInvestmentStats(List<BudgetValue> budgetValues, decimal initialInvestmentTotal)
        {
            var investmentStats = new List<InvestmentStats>();

            var investmentListCopy = ObjectCopier.Clone(GetTypeList("Investment", budgetValues));
            foreach (var item in investmentListCopy)
            {
                item.TotalInvestmentAmount = item.Amount;
            }

            var totalInvestment = initialInvestmentTotal;            
            var investmentCount = investmentListCopy.Count();

            var year = 1;
            while (year <= 20) 
            {
                var lastTotalInvestment = totalInvestment;
                var totalInvestmentPlusInterest = totalInvestment;

                foreach (var item in investmentListCopy)
                {
                    var interestRate = item.InterestRateVal / 100;
                    totalInvestmentPlusInterest += item.TotalInvestmentAmount * interestRate;
                    item.TotalInvestmentAmount += (item.TotalInvestmentAmount * interestRate)
                        + (investmentCount == 0 ? 0 : (yearlySavings / investmentCount));
                }

                var totalInvestmentWithSavings = totalInvestmentPlusInterest + yearlySavings;
                var totalInvestmentNoSavings = totalInvestmentPlusInterest;

                totalInvestment = totalInvestmentWithSavings;
                
                investmentStats.Add(new InvestmentStats
                    {
                        YearPerDayAmount = (totalInvestmentNoSavings - lastTotalInvestment) / 365,
                        YearInvestmentTotal = totalInvestmentWithSavings
                    }
                );

                year++;
            }

            return investmentStats;
        }

        private List<InvestmentStats> GetPreTaxInvestmentStats(List<BudgetValue> budgetValues, decimal initialPreTaxInvestmentTotal)
        {   
            var preTaxInvestmentStats = new List<InvestmentStats>();

            var preTaxInvestmentListCopy = ObjectCopier.Clone(GetTypeList("PreTaxInvestment", budgetValues));
            foreach (var item in preTaxInvestmentListCopy)
            {
                item.TotalPreTaxInvestmentAmount = item.Amount;
            }
            
            var totalPreTaxInvestment = initialPreTaxInvestmentTotal;

            var year = 1;
            while (year <= 20) 
            {
                var lastTotalPreTaxInvestment = totalPreTaxInvestment;
                var totalPreTaxInvestmentPlusInterest = totalPreTaxInvestment;
                decimal yearlyPreTaxContributions = 0;
                var preTaxInvestmentContributionList = GetTypeList("PreTaxInvestmentContribution", budgetValues);

                foreach (var item in preTaxInvestmentListCopy)
                {
                    var interestRate = item.InterestRateVal / 100;
                    totalPreTaxInvestmentPlusInterest += (item.TotalPreTaxInvestmentAmount * interestRate);

                    var name = item.Name;
                    var yearlyPreTaxContribution = preTaxInvestmentContributionList.First(x => x.Name == name).Amount * 12;

                    item.TotalPreTaxInvestmentAmount += (item.TotalPreTaxInvestmentAmount * interestRate)
                        + yearlyPreTaxContribution;
                    yearlyPreTaxContributions += yearlyPreTaxContribution;
                }

                var totalPreTaxInvestmentWithContributions = totalPreTaxInvestmentPlusInterest
                    + yearlyPreTaxContributions;
                var totalPreTaxInvestmentNoContributions = totalPreTaxInvestmentPlusInterest;

                totalPreTaxInvestment = totalPreTaxInvestmentWithContributions;

                preTaxInvestmentStats.Add(new InvestmentStats
                    {
                        YearPerDayAmount = (totalPreTaxInvestmentNoContributions - lastTotalPreTaxInvestment) / 365,
                        YearInvestmentTotal = totalPreTaxInvestmentWithContributions
                    }
                );

                year++;                
            }

            return preTaxInvestmentStats;
        }
    }
}