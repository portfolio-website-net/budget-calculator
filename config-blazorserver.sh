#!/bin/bash

# Remove Blazor WebAssembly files
rm -f budget-calculator.csproj
rm -f Program.cs
rm -f App.razor
rm -f _Imports.razor

# Copy Blazor Server files
cp BlazorServer/Pages/_Host.cshtml-renamed ./Pages/_Host.cshtml
cp BlazorServer/blazorserver-budget-calculator.csproj ./blazorserver-budget-calculator.csproj
cp BlazorServer/BlazorServerApp.razor-renamed ./BlazorServerApp.razor
cp BlazorServer/BlazorServerProgram.cs-renamed ./BlazorServerProgram.cs
cp BlazorServer/BlazorServerStartup.cs-renamed ./BlazorServerStartup.cs
cp BlazorServer/_Imports.razor-renamed ./_Imports.razor
cp BlazorServer/appsettings.json appsettings.json
cp BlazorServer/appsettings.Development.json appsettings.Development.json