#!/bin/bash

# Remove Blazor Server files
rm -f Pages/_Host.cshtml
rm -f blazorserver-budget-calculator.csproj
rm -f BlazorServerApp.razor
rm -f BlazorServerProgram.cs
rm -f BlazorServerStartup.cs
rm -f appsettings.json
rm -f appsettings.Development.json

# Copy Blazor WebAssembly files
cp BlazorWebAssembly/budget-calculator.csproj budget-calculator.csproj
cp BlazorWebAssembly/Program.cs-renamed Program.cs
cp BlazorWebAssembly/App.razor-renamed App.razor
cp BlazorWebAssembly/_Imports.razor-renamed _Imports.razor