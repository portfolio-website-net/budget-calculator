var currencyAutoNumeric;
var percentageAutoNumeric;

window.helperJsFunctions = {
    initializeModal: function() {
        $('#dialog-form').modal({
            keyboard: true,
            backdrop: 'static',
            show: false
        })

        $('#dialog-form').on('shown.bs.modal', function (e) {
            $('#txtCategoryName').focus()
        })
    },

    initializeAutoNumeric: function () {

        if (currencyAutoNumeric && currencyAutoNumeric.length) {
            $.each(currencyAutoNumeric, function () {
                this.remove()
            })
        }

        if (percentageAutoNumeric && percentageAutoNumeric.length) {
            $.each(percentageAutoNumeric, function () {
                this.remove()
            })
        }

        currencyAutoNumeric = new AutoNumeric.multiple('input.currency', 'NorthAmerican')
        percentageAutoNumeric = new AutoNumeric.multiple('input.percentage', 'percentageUS2decPos')
    },

    initializeIcons: function () {
        feather.replace()
    },

    showModal: function() {
        $('#dialog-form').modal('show')
    },

    hideModal: function() {
        $('#dialog-form').modal('hide')
    },
}