// Helper Functions

// Source URL: https://stackoverflow.com/questions/149055/how-can-i-format-numbers-as-dollars-currency-string-in-javascript/149099#149099
Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
    c = isNaN(c = Math.abs(c)) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
    j = (j = i.length) > 3 ? j % 3 : 0
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "")
}

// Reference URL: https://stackoverflow.com/questions/2010892/storing-objects-in-html5-localstorage/3146971#3146971
Storage.prototype.setObject = function (key, value) {
    this.setItem(key, JSON.stringify(value))
}

Storage.prototype.getObject = function (key) {
    let value = this.getItem(key)
    return value && JSON.parse(value)
}

function copyObj(obj) {
    return JSON.parse(JSON.stringify(obj))
}

function formatValue(value, isPercent) {
    let currencySymbol = '$'
    let percentSymbol = ''

    if (isPercent) {
        percentSymbol = '%'
        currencySymbol = ''
    }

    return currencySymbol + value.formatMoney(2) + percentSymbol
}

function convertNameToCode(name) {
    return name.replace(/\W/g, '')
}

function initializeIcons() {
    feather.replace()
}

function getDefaultBudgetValues() {
    let values = [
        { type: 'Expense', name: 'Rent', amount: 1500 },
        { type: 'Expense', name: 'Electricity', amount: 75 },
        { type: 'Expense', name: 'Internet', amount: 80 },
        { type: 'Expense', name: 'Phone', amount: 100 },
        { type: 'Expense', name: 'Groceries', amount: 400 },
        { type: 'Expense', name: 'Insurance', amount: 120 },
        { type: 'Expense', name: 'Clothing', amount: 100 },
        { type: 'Expense', name: 'Entertainment', amount: 500 },
        { type: 'Expense', name: 'Vacation', amount: 2000 },
        { type: 'Expense', name: 'Fuel', amount: 100 },
        { type: 'Expense', name: 'Vehicle Maintenance', amount: 100 },
        { type: 'Expense', name: 'Fitness', amount: 50 },
        { type: 'Expense', name: 'Haircut', amount: 15 },

        { type: 'Income', name: 'Paycheck', amount: 8000 },

        { type: 'Investment', name: '1 Year CD', interestRate: 3, amount: 180000 },
        { type: 'Investment', name: 'Savings', interestRate: 2, amount: 15000 },

        { type: 'PreTaxInvestment', name: '401k', interestRate: 6, amount: 400000 },
        { type: 'PreTaxInvestment', name: 'Traditional IRA', interestRate: 5, amount: 100000 },
        { type: 'PreTaxInvestment', name: 'HSA', interestRate: 2, amount: 15000 },

        { type: 'PreTaxInvestmentContribution', name: '401k', amount: 2000 },
        { type: 'PreTaxInvestmentContribution', name: 'Traditional IRA', amount: 250 },
        { type: 'PreTaxInvestmentContribution', name: 'HSA', amount: 200 }
    ]        

    return values
}

$(function () {

    $('#dialog-form').modal({
        keyboard: true,
        backdrop: 'static',
        show: false
    })

    $('#dialog-form').on('shown.bs.modal', function (e) {
        $('#txtCategoryName').focus()
    })

})

var budgetValues = []

function initialize() {
    
    budgetValues = localStorage.getObject('BudgetValues')
    if (budgetValues == null || budgetValues.length == 0) {
        budgetValues = getDefaultBudgetValues()
    }

    $.each(budgetValues, function () {
        this.code = convertNameToCode(this.name)
    })
}

initialize()

let htmlCardTemplate = [
    '<div>',
        '<div class="card">',
            '<h5 class="card-header blue-header">{{ title }}</h5>',
            '<div class="card-body">',
                '<p class="card-text">',
                    '<slot name="description"></slot>',
                '</p>',
                '<edit-table v-bind:type="type" v-bind:typeList="typeList"></edit-table>',
                '<display-currency-total-table v-bind:typeTotalsList="typeTotalsList"></display-currency-total-table>',
            '</div>',
        '</div>',
        '<br />',
    '</div>'
].join('')

let htmlCardTemplateWithInterestRate = [
    '<div>',
        '<div class="card">',
            '<h5 class="card-header blue-header">{{ title }}</h5>',
            '<div class="card-body">',
                '<p class="card-text">',
                    '<slot name="description"></slot>',
                '</p>',
                '<edit-table-with-interest-rate v-bind:type="type" v-bind:typeList="typeList"></edit-table-with-interest-rate>',
                '<display-currency-total-table v-bind:typeTotalsList="typeTotalsList"></display-currency-total-table>',
            '</div>',
        '</div>',
        '<br />',
    '</div>'
].join('')

let htmlCardTemplateWithTotalsOnly = [
    '<div>',
        '<div class="card">',
            '<h5 class="card-header blue-header">{{ title }}</h5>',
            '<div class="card-body">',
                '<display-total-table v-bind:typeTotalsList="typeTotalsList"></display-total-table>',
            '</div>',
        '</div>',
        '<br />',
    '</div>'
].join('')

let htmlEditTableTemplate = [
    '<table class="table table-striped table-bordered table-hover table-sm">',
        '<tbody>',
            '<tr is="edit-row-item" v-for="item in typeList" v-bind:item="item"></tr>',
        '</tbody>',
        '<tfoot>',
            '<tr>',
                '<td colspan="3" style="text-align: right;">',
                    '<button type="button" class="btn btn-primary btn-sm" @click="emitShowAddCategoryDialog(type)"><span data-feather="plus-circle"></span> Add</button>',
                '</td>',
            '</tr>',
        '</tfoot>',
    '</table>'
].join('')

let htmlEditItemRowTemplate = [
    '<tr>',
        '<td>',
            '<span>{{ item.name }}</span>',
        '</td>',
        '<td>',
            '<vue-autonumeric v-model="item.amount" v-bind:options="{ currencySymbol: \'$\' }" v-bind:placeholder="item.name" class="form-control"></vue-autonumeric>',
        '</td>',
        '<td>',
            '<button type="button" class="btn btn-danger btn-sm" @click="emitRemoveCategory(item.type, item.code)"><span data-feather="x-circle"></span></button>',
        '</td>',
    '</tr>'
].join('')

let htmlEditTableWithInterestRateTemplate = [
    '<table class="table table-striped table-bordered table-hover table-sm">',
        '<thead>',
            '<tr>',
                '<th>',
                    'Name',
                '</th>',
                '<th>',
                    'Interest Rate',
                '</th>',
                '<th>',
                    'Amount',
                '</th>',
                '<th></th>',
            '</tr>',
        '</thead>',
        '<tbody>',
            '<tr is="edit-row-item-with-interest-rate" v-for="item in typeList" v-bind:item="item"></tr>',
        '</tbody>',
        '<tfoot>',
            '<tr>',
                '<td colspan="4" style="text-align: right;">',
                    '<button type="button" class="btn btn-primary btn-sm" @click="emitShowAddCategoryDialog(type)"><span data-feather="plus-circle"></span> Add</button>',
                '</td>',
            '</tr>',
        '</tfoot>',
    '</table>'
].join('')

let htmlEditItemWithInterestRateRowTemplate = [
    '<tr>',
        '<td>',
            '<span>{{ item.name }}</span>',
        '</td>',
        '<td>',
            '<vue-autonumeric v-model="item.interestRate" v-bind:options="{ currencySymbol: \'%\', currencySymbolPlacement: \'s\' }" placeholder="%" class="form-control"></vue-autonumeric>',
        '</td>',
        '<td>',
            '<vue-autonumeric v-model="item.amount" v-bind:options="{ currencySymbol: \'$\' }" v-bind:placeholder="item.name" class="form-control"></vue-autonumeric>',
        '</td>',
        '<td>',
            '<button type="button" class="btn btn-danger btn-sm" @click="emitRemoveCategory(item.type, item.code)"><span data-feather="x-circle"></span></button>',
        '</td>',
    '</tr>'
].join('')

let htmlDisplayCurrencyTotalTableTemplate = [
    '<table class="table table-striped table-bordered table-hover table-sm">',
        '<tbody>',
            '<tr is="display-currency-total-row-item" v-for="item in typeTotalsList" v-bind:item="item"></tr>',
        '</tbody>',
    '</table>'
].join('')

let htmlDisplayCurrencyTotalRowTemplate = [
    '<tr>',
        '<td>',
            '<span>{{ item.name }}</span>',
        '</td>',
        '<td>',
            '<span>${{ item.amount.formatMoney(2) }}</span>',
        '</td>',
    '</tr>'
].join('')

let htmlDisplayTotalTableTemplate = [
    '<table class="table table-striped table-bordered table-hover table-sm">',
        '<tbody>',
            '<tr is="display-total-row-item" v-for="item in typeTotalsList" v-bind:item="item"></tr>',
        '</tbody>',
    '</table>'
].join('')

let htmlDisplayTotalRowTemplate = [
    '<tr>',
        '<td>',
            '<span>{{ item.name }}</span>',
        '</td>',
        '<td>',
            '<span>{{ item.amount }}</span>',
        '</td>',
    '</tr>'
].join('')

const bus = new Vue({})

Vue.component('card-panel', {
    props: [
        'title',
        'type',
        'typeList',
        'typeTotalsList'
    ],
    template: htmlCardTemplate
})

Vue.component('card-panel-with-interest-rate', {
    props: [
        'title',
        'type',
        'typeList',
        'typeTotalsList'
    ],
    template: htmlCardTemplateWithInterestRate
})

Vue.component('card-panel-with-totals-only', {
    props: [
        'title',
        'typeTotalsList'
    ],
    template: htmlCardTemplateWithTotalsOnly
})

Vue.component('edit-table', {
    props: [
        'type',
        'typeList'
    ],
    template: htmlEditTableTemplate,
    methods: {
        emitShowAddCategoryDialog(type) {
            bus.$emit('show-add-category-dialog', type)
        }
    }
})

Vue.component('edit-row-item', {
    props: ['item'],
    template: htmlEditItemRowTemplate,
    methods: {
        emitRemoveCategory(type, code) {
            bus.$emit('remove-category', type, code)
        }
    }
})

Vue.component('edit-table-with-interest-rate', {
    props: [
        'type',
        'typeList'
    ],
    template: htmlEditTableWithInterestRateTemplate,
    methods: {
        emitShowAddCategoryDialog(type) {
            bus.$emit('show-add-category-dialog', type)
        }
    }
})

Vue.component('edit-row-item-with-interest-rate', {
    props: ['item'],
    template: htmlEditItemWithInterestRateRowTemplate,
    methods: {
        emitRemoveCategory(type, code) {
            bus.$emit('remove-category', type, code)
        }
    }
})

Vue.component('display-currency-total-table', {
    props: ['typeTotalsList'],
    template: htmlDisplayCurrencyTotalTableTemplate
})

Vue.component('display-currency-total-row-item', {
    props: ['item'],
    template: htmlDisplayCurrencyTotalRowTemplate
})

Vue.component('display-total-table', {
    props: ['typeTotalsList'],
    template: htmlDisplayTotalTableTemplate
})

Vue.component('display-total-row-item', {
    props: ['item'],
    template: htmlDisplayTotalRowTemplate
})
        
var app = new Vue({
    el: '#app',
    data: {
        dialogValues: {
            type: '',
            title: '',
            name: '',
            showNameError: false,
            showInterestRate: false,
            interestRate: '',
            amount: ''
        },
        dataList: budgetValues
    },
    components: {
        VueAutonumeric
    },
    created() {
        bus.$on('remove-category', (type, code) => {
            this.removeCategory(type, code)
        })

        bus.$on('show-add-category-dialog', (type) => {
            this.showAddCategoryDialog(type)
        })
    },
    methods: {
        removeCategory: function (type, code, ignoreMirroringPreTaxInvestmentRow) {
            if (ignoreMirroringPreTaxInvestmentRow || confirm('Are you sure you want to remove this line item?')) {
                this.dataList = this.dataList.filter(function (el) {
                    return el.type !== type || el.code !== code
                })

                if (!ignoreMirroringPreTaxInvestmentRow) {
                    if (type == 'PreTaxInvestment') {
                        this.removeCategory('PreTaxInvestmentContribution', code, true)
                    }
                    else if (type == 'PreTaxInvestmentContribution') {
                        this.removeCategory('PreTaxInvestment', code, true)
                    }
                }
            }
        },
        showAddCategoryDialog: function (type) {
            this.dialogValues = {
                type: '',
                title: '',
                name: '',
                showNameError: false,
                showInterestRate: false,
                interestRate: '',
                amount: ''
            }

            if (type == 'Expense') {
                this.dialogValues.title = 'Add Expense Category'
            }
            else if (type == 'Income') {
                this.dialogValues.title = 'Add Income Category'
            }
            else if (type == 'Investment') {
                this.dialogValues.title = 'Add Investment Category'
                this.dialogValues.showInterestRate = true
            }
            else if (type == 'PreTaxInvestment') {
                this.dialogValues.title = 'Add Pre-Tax Investment Category'
                this.dialogValues.showInterestRate = true
            }
            else if (type == 'PreTaxInvestmentContribution') {
                this.dialogValues.title = 'Add Pre-Tax Contribution Category'
            }

            this.dialogValues.type = type
            $('#dialog-form').modal('show')
        },
        addCategory: function (type, name, code, interestRate, amount, ignoreMirroringPreTaxInvestmentRow) {
            // Remove any existing items matching the new category
            this.dataList = this.dataList.filter(function (el) {
                return el.type !== type || el.code !== code
            })

            this.dataList.push({
                type: type,
                name: name,
                code: code,
                interestRate: interestRate,
                amount: amount
            })

            if (!ignoreMirroringPreTaxInvestmentRow) {
                if (type == 'PreTaxInvestment') {
                    this.addCategory('PreTaxInvestmentContribution', name, code, 0, 0, true)
                }
                else if (type == 'PreTaxInvestmentContribution') {
                    this.addCategory('PreTaxInvestment', name, code, 0, 0, true)
                }
            }
        },
        addCategoryType: function () {
            let name = this.dialogValues.name
            if (name) {
                let type = this.dialogValues.type
                let code = convertNameToCode(this.dialogValues.name)
                let amount = this.dialogValues.amount
                let interestRate = this.dialogValues.interestRate                    

                this.addCategory(type, name, code, interestRate, amount)

                $('#dialog-form').modal('hide')
            }
            else {
                this.dialogValues.showNameError = true
            }
        },
        clearBudgetValues: function () {
            if (confirm('Are you sure you want to clear the budget values and start over?')) {
                this.dataList = []
            }
        },
        resetBudgetValuesToExampleDefaults: function () {
            if (confirm('Are you sure you want to reset the budget values to the example defaults?')) {
                let budgetValues = getDefaultBudgetValues()

                $.each(budgetValues, function () {
                    this.code = convertNameToCode(this.name)
                })

                this.dataList = budgetValues
            }
        },
        calculateTotals: function (type) {
            let dataTotalsList = []

            let expenseTotal = this.expenseList.reduce((sum, item) => {
                return sum + item.amount
            }, 0)

            let incomeTotal = this.incomeList.reduce((sum, item) => {
                return sum + item.amount
            }, 0)

            let initialInvestmentTotal = this.investmentList.reduce((sum, item) => {
                return sum + item.amount
            }, 0)

            let yearlySavings = (incomeTotal - expenseTotal) * 12
            
            let investmentListCopy = copyObj(this.investmentList)
            $.each(investmentListCopy, function () {
                this.totalInvestmentAmount = this.amount
            })
            
            let year = 1
            let investmentStats = []
            let totalInvestment = initialInvestmentTotal
            let investmentCount = investmentListCopy.length

            while (year <= 20) {
                let lastTotalInvestment = totalInvestment
                let totalInvestmentPlusInterest = totalInvestment

                $.each(investmentListCopy, function () {
                    let interestRate = this.interestRate / 100
                    totalInvestmentPlusInterest += this.totalInvestmentAmount * interestRate
                    this.totalInvestmentAmount += (this.totalInvestmentAmount * interestRate)
                        + (yearlySavings / investmentCount)
                })

                var totalInvestmentWithSavings = totalInvestmentPlusInterest + yearlySavings
                var totalInvestmentNoSavings = totalInvestmentPlusInterest

                totalInvestment = totalInvestmentWithSavings
                
                investmentStats.push({
                    yearPerDayAmount: (totalInvestmentNoSavings - lastTotalInvestment) / 365,
                    yearInvestmentTotal: totalInvestmentWithSavings
                })

                year++
            }

            let initialPreTaxInvestmentTotal = this.preTaxInvestmentList.reduce((sum, item) => {
                return sum + item.amount
            }, 0)

            let preTaxInvestmentListCopy = copyObj(this.preTaxInvestmentList)
            $.each(preTaxInvestmentListCopy, function () {
                this.totalPreTaxInvestmentAmount = this.amount
            })

            year = 1
            let preTaxInvestmentStats = []
            let totalPreTaxInvestment = initialPreTaxInvestmentTotal

            while (year <= 20) {
                let lastTotalPreTaxInvestment = totalPreTaxInvestment
                let totalPreTaxInvestmentPlusInterest = totalPreTaxInvestment
                let yearlyPreTaxContributions = 0
                let preTaxInvestmentContributionList = this.preTaxInvestmentContributionList

                $.each(preTaxInvestmentListCopy, function () {
                    let interestRate = this.interestRate / 100
                    totalPreTaxInvestmentPlusInterest += (this.totalPreTaxInvestmentAmount * interestRate)

                    let code = this.code
                    let yearlyPreTaxContribution = preTaxInvestmentContributionList.filter(function (el) {
                        return el.code === code
                    })[0].amount * 12

                    this.totalPreTaxInvestmentAmount += (this.totalPreTaxInvestmentAmount * interestRate)
                        + yearlyPreTaxContribution
                    yearlyPreTaxContributions += yearlyPreTaxContribution
                })

                var totalPreTaxInvestmentWithContributions = totalPreTaxInvestmentPlusInterest
                    + yearlyPreTaxContributions
                var totalPreTaxInvestmentNoContributions = totalPreTaxInvestmentPlusInterest

                totalPreTaxInvestment = totalPreTaxInvestmentWithContributions                    

                preTaxInvestmentStats.push({
                    yearPerDayAmount: (totalPreTaxInvestmentNoContributions - lastTotalPreTaxInvestment) / 365,
                    yearInvestmentTotal: totalPreTaxInvestmentWithContributions
                })

                year++
            }

            let preTaxInvestmentContributionTotal = this.preTaxInvestmentContributionList.reduce((sum, item) => {
                return sum + item.amount
            }, 0)

            if (type === 'Expense') {
                dataTotalsList.push({ type: 'Expense', name: 'Monthly Expenses Total', amount: expenseTotal })

                $.each([1, 5, 10, 15, 20], function () {
                    dataTotalsList.push(
                        {
                            type: 'Expense',
                            name: this + ' Year Expenses',
                            amount: expenseTotal * 12 * this
                        })
                })
            }
            else if (type === 'Income') {
                dataTotalsList.push(
                    {
                        type: 'Income',
                        name: 'Monthly Income Total',
                        amount: incomeTotal
                    })

                $.each([1, 5, 10, 15, 20], function () {
                    dataTotalsList.push(
                        {
                            type: 'Income',
                            name: this + ' Year Income',
                            amount: incomeTotal * 12 * this
                        })
                })
            }
            else if (type === 'Investment') {
                dataTotalsList.push(
                    {
                        type: 'Investment',
                        name: 'Initial Investment Total',
                        amount: initialInvestmentTotal
                    })

                year = 1
                while (year <= 20) {
                    if (year == 1 || year == 5 || year == 10 || year == 15 || year == 20) {
                        let investmentStat = investmentStats[year - 1]
                        dataTotalsList.push(
                            {
                                type: 'Investment',
                                name: year + ' Year - Earned Per Day',
                                amount: investmentStat.yearPerDayAmount
                            })
                        dataTotalsList.push(
                            {
                                type: 'Investment',
                                name: year + ' Year Investment Total',
                                amount: investmentStat.yearInvestmentTotal
                            })
                    }

                    year++
                }                    
            }
            else if (type === 'PreTaxInvestment') {
                dataTotalsList.push(
                    {
                        type: 'PreTaxInvestment',
                        name: 'Initial Pre-Tax Investment Total',
                        amount: initialPreTaxInvestmentTotal
                    })
                
                year = 1
                while (year <= 20) {
                    if (year == 1 || year == 5 || year == 10 || year == 15 || year == 20) {
                        let preTaxinvestmentStat = preTaxInvestmentStats[year - 1]
                        dataTotalsList.push(
                            {
                                type: 'PreTaxInvestment',
                                name: year + ' Year - Earned Per Day',
                                amount: preTaxinvestmentStat.yearPerDayAmount
                            })
                        dataTotalsList.push(
                            {
                                type: 'PreTaxInvestment',
                                name: year + ' Year Pre-Tax Investment Total',
                                amount: preTaxinvestmentStat.yearInvestmentTotal
                            })
                    }

                    year++
                }
            }
            else if (type === 'PreTaxInvestmentContribution') {
                dataTotalsList.push(
                    {
                        type: 'PreTaxInvestment',
                        name: 'Monthly Pre-Tax Contribution Total',
                        amount: preTaxInvestmentContributionTotal
                    })
                
                $.each([1, 5, 10, 15, 20], function () {
                    dataTotalsList.push(
                        {
                            type: 'PreTaxInvestment',
                            name: this + ' Year Pre-Tax Contribution (No Interest)',
                            amount: preTaxInvestmentContributionTotal * 12 * this
                        })
                })
            }
            else if (type === 'Savings') {
                dataTotalsList.push(
                    {
                        type: 'Savings',
                        name: 'Hourly Savings',
                        amount: formatValue((yearlySavings / 365) / 24)
                    })
                dataTotalsList.push(
                    {
                        type: 'Savings',
                        name: 'Daily Savings',
                        amount: formatValue(yearlySavings / 365)
                    })
                dataTotalsList.push(
                    {
                        type: 'Savings',
                        name: 'Monthly Savings',
                        amount: formatValue(incomeTotal - expenseTotal)
                    })

                $.each([1, 5, 10, 15, 20], function () {
                    dataTotalsList.push(
                        {
                            type: 'Savings',
                            name: this + ' Year Savings',
                            amount: formatValue(yearlySavings * this)
                        })
                })

                dataTotalsList.push(
                    {
                        type: 'Savings',
                        name: '% Savings from Monthly Income',
                        amount: formatValue(((incomeTotal - expenseTotal) / incomeTotal) * 100, true)
                    })
                
                dataTotalsList.push(
                    {
                        type: 'Savings',
                        name: 'Monthly Savings w/ Pre-Tax (No Interest)',
                        amount: formatValue((incomeTotal - expenseTotal) 
                            + preTaxInvestmentContributionTotal)
                    })
                
                $.each([1, 5, 10, 15, 20], function () {
                    dataTotalsList.push(
                        {
                            type: 'Savings',
                            name: this + ' Year Savings w/ Pre-Tax (No Interest)',
                            amount: formatValue(((incomeTotal - expenseTotal)
                                + preTaxInvestmentContributionTotal) * 12 * this)
                        })
                })

                $.each([1, 5, 10, 15, 20], function () {
                    dataTotalsList.push(
                        {
                            type: 'Savings',
                            name: this + ' Year Savings w/ Pre-Tax w/ Interest - Earned Per Day',
                            amount: formatValue(investmentStats[this - 1].yearPerDayAmount
                                + preTaxInvestmentStats[this - 1].yearPerDayAmount)
                        })
                    dataTotalsList.push(
                        {
                            type: 'Savings',
                            name: this + ' Year Savings w/ Pre-Tax w/ Interest',
                            amount: formatValue(investmentStats[this - 1].yearInvestmentTotal
                                + preTaxInvestmentStats[this - 1].yearInvestmentTotal)
                        })
                })
            }

            return dataTotalsList
        }
    },
    computed: {
        expenseList: function () {
            return this.dataList.filter(function (el) {
                return el.type === 'Expense'
            })
        },
        expenseTotalsList: function () {                
            return this.calculateTotals('Expense')
        },
        incomeList: function () {
            return this.dataList.filter(function (el) {
                return el.type === 'Income'
            })
        },
        incomeTotalsList: function () {
            return this.calculateTotals('Income')
        },
        investmentList: function () {
            return this.dataList.filter(function (el) {
                return el.type === 'Investment'
            })
        },
        investmentTotalsList: function () {
            return this.calculateTotals('Investment')
        },
        preTaxInvestmentList: function () {
            return this.dataList.filter(function (el) {
                return el.type === 'PreTaxInvestment'
            })
        },
        preTaxInvestmentTotalsList: function () {
            return this.calculateTotals('PreTaxInvestment')
        },
        preTaxInvestmentContributionList: function () {
            return this.dataList.filter(function (el) {
                return el.type === 'PreTaxInvestmentContribution'
            })
        },
        preTaxInvestmentContributionTotalsList: function () {
            return this.calculateTotals('PreTaxInvestmentContribution')
        },
        savingsTotalsList: function () {
            return this.calculateTotals('Savings')
        }
    },
    updated: function () {
        this.$nextTick(function () {
            initializeIcons()
            localStorage.setObject('BudgetValues', this.dataList)
        })
    }
})

$('#loader').hide()
$('#app').show()
initializeIcons()