// Helper Functions

// Source URL: https://stackoverflow.com/questions/149055/how-can-i-format-numbers-as-dollars-currency-string-in-javascript/149099#149099
Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
    c = isNaN(c = Math.abs(c)) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
    j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function cleanValue(value) {
    var cleanedValue = value.replace(/\$/g, '').replace(/,/g, '');

    if (!cleanedValue) {
        cleanedValue = 0;
    }

    return cleanedValue;
}

// Reference URL: https://stackoverflow.com/questions/2010892/storing-objects-in-html5-localstorage/3146971#3146971
Storage.prototype.setObject = function (key, value) {
    this.setItem(key, JSON.stringify(value));
}

Storage.prototype.getObject = function (key) {
    var value = this.getItem(key);
    return value && JSON.parse(value);
}

function initializeIcons() {
    feather.replace()
}

// Expense Category Functions
function addExpenseCategory(name, value) {

    var code = convertNameToCode(name);
    removeExpenseCategory(code);

    $('#tblExpenseCategories tbody').append(
        '<tr class="expense-category" data-code="' + code + '">'
            + '<td>'
                + '<span class="category-name">' + name + '</span>'
            + '</td>'
            + '<td>'
                + '<input type="text" class="form-control currency category-amount" placeholder="' + name + '" value="' + value + '" />'
            + '</td>'
            + '<td>'
                + '<button type="button" class="btn btn-danger btn-sm" onclick="removeExpenseCategory(\'' + code + '\', true);"><span data-feather="x-circle"></span></button>'
            + '</td>'
        + '</tr>');
            
    initializeChangeListeners();

    calculateTotals();
}

function removeExpenseCategory(code, useConfirmation) {
    if (!useConfirmation || confirm('Are you sure you want to remove this line item?')) {
        $('.expense-category[data-code="' + code + '"]').remove();

        calculateTotals();
    }        
}

function showAddExpenseCategory() {
    $('#dialog-form .modal-title').html('Add Expense Category');
    $('#dialog-form #hdnCategoryType').val('Expense');
    $('#dialog-form').modal('show');
}

function addExpenseTotal(name, value) {
    var code = convertNameToCode(name);
    removeExpenseTotal(code);

    $('#tblExpenseTotals tbody').append(
        '<tr class="expense-total" data-code="' + code + '">'
            + '<td>'
                + '<span>' + name + '</span>'
            + '</td>'
            + '<td>'
                + '<span>$' + value.formatMoney(2) + '</span>'
            + '</td>'
        + '</tr>');
}

function removeExpenseTotal(code) {
    $('.expense-total[data-code="' + code + '"]').remove();
}

// Income Category Functions
function addIncomeCategory(name, value) {
    var code = convertNameToCode(name);
    removeIncomeCategory(code);

    $('#tblIncomeCategories tbody').append(
        '<tr class="income-category" data-code="' + code + '">'
            + '<td>'
                + '<span class="category-name">' + name + '</span>'
            + '</td>'
            + '<td>'
                + '<input type="text" class="form-control currency category-amount" placeholder="' + name + '" value="' + value + '" />'
            + '</td>'
            + '<td>'
                + '<button type="button" class="btn btn-danger btn-sm" onclick="removeIncomeCategory(\'' + code + '\', true);"><span data-feather="x-circle"></span></button>'
            + '</td>'
        + '</tr>');
            
    initializeChangeListeners();

    calculateTotals();
}

function removeIncomeCategory(code, useConfirmation) {
    if (!useConfirmation || confirm('Are you sure you want to remove this line item?')) {
        $('.income-category[data-code="' + code + '"]').remove();

        calculateTotals();
    }
}

function showAddIncomeCategory() {
    $('#dialog-form .modal-title').html('Add Income Category');
    $('#dialog-form #hdnCategoryType').val('Income');
    $('#dialog-form').modal('show');
}

function addIncomeTotal(name, value) {
    var code = convertNameToCode(name);
    removeIncomeTotal(code);

    $('#tblIncomeTotals tbody').append(
        '<tr class="income-total" data-code="' + code + '">'
            + '<td>'
                + '<span>' + name + '</span>'
            + '</td>'
            + '<td>'
                + '<span>$' + value.formatMoney(2) + '</span>'
            + '</td>'
        + '</tr>');
}

function removeIncomeTotal(code) {
    $('.income-total[data-code="' + code + '"]').remove();
}

// Investment Category Functions
function addInvestmentCategory(name, interestRate, value) {
    var code = convertNameToCode(name);
    removeInvestmentCategory(code);

    $('#tblInvestmentCategories tbody').append(
        '<tr class="investment-category" data-code="' + code + '">'
            + '<td>'
                + '<span class="category-name">' + name + '</span>'
            + '</td>'
            + '<td>'
                + '<input type="text" class="form-control percentage category-interestrate" placeholder="' + name + '" value="' + interestRate + '" />'
            + '</td>'
            + '<td>'
                + '<input type="text" class="form-control currency category-amount" placeholder="' + name + '" value="' + value + '" />'
            + '</td>'
            + '<td>'
                + '<button type="button" class="btn btn-danger btn-sm" onclick="removeInvestmentCategory(\'' + code + '\', true);"><span data-feather="x-circle"></span></button>'
            + '</td>'
        + '</tr>');

    initializeChangeListeners();

    calculateTotals();
}

function removeInvestmentCategory(code, useConfirmation) {
    if (!useConfirmation || confirm('Are you sure you want to remove this line item?')) {
        $('.investment-category[data-code="' + code + '"]').remove();

        calculateTotals();
    }
}

function showAddInvestmentCategory() {
    $('#dialog-form .modal-title').html('Add Investment Category');
    $('#dialog-form #hdnCategoryType').val('Investment');
    $('#dialog-form #pnlCategoryInterestRate').show();
    $('#dialog-form').modal('show');
}

function addInvestmentTotal(name, value) {
    var code = convertNameToCode(name);
    removeInvestmentTotal(code);

    $('#tblInvestmentTotals tbody').append(
        '<tr class="investment-total" data-code="' + code + '">'
            + '<td>'
                + '<span>' + name + '</span>'
            + '</td>'
            + '<td>'
                + '<span>$' + value.formatMoney(2) + '</span>'
            + '</td>'
        + '</tr>');
}

function removeInvestmentTotal(code) {
    $('.investment-total[data-code="' + code + '"]').remove();
}

// Pre-Tax Investment Category Functions
function addPreTaxInvestmentCategory(name, interestRate, value, isAutoPopulate) {     
    var code = convertNameToCode(name);   
    var originalName = name;
    removePreTaxInvestmentCategory(code);

    $('#tblPreTaxInvestmentCategories tbody').append(
        '<tr class="pretaxinvestment-category" data-code="' + code + '">'
            + '<td>'
                + '<span class="category-name">' + name + '</span>'
            + '</td>'
            + '<td>'
                + '<input type="text" class="form-control percentage category-interestrate" placeholder="' + name + '" value="' + interestRate + '" />'
            + '</td>'
            + '<td>'
                + '<input type="text" class="form-control currency category-amount" placeholder="' + name + '" value="' + value + '" />'
            + '</td>'
            + '<td>'
                + '<button type="button" class="btn btn-danger btn-sm" onclick="removePreTaxInvestmentCategory(\'' + code + '\', true);"><span data-feather="x-circle"></span></button>'
            + '</td>'
        + '</tr>');
            
    initializeChangeListeners();

    if (isAutoPopulate) {
        addPreTaxInvestmentContributionCategory(originalName, 0);
    }

    calculateTotals();
}

function removePreTaxInvestmentCategory(code, useConfirmation) {
    if (!useConfirmation || confirm('Are you sure you want to remove this line item?')) {
        $('.pretaxinvestment-category[data-code="' + code + '"]').remove();

        if (useConfirmation) {
            $('.pretaxinvestmentcontribution-category[data-code="' + code + '"]').remove();
        }

        calculateTotals();
    }
}

function showAddPreTaxInvestmentCategory() {
    $('#dialog-form .modal-title').html('Add Pre-Tax Investment Category');
    $('#dialog-form #hdnCategoryType').val('PreTaxInvestment');
    $('#dialog-form #pnlCategoryInterestRate').show();
    $('#dialog-form').modal('show');
}

function addPreTaxInvestmentTotal(name, value) {
    var code = convertNameToCode(name);
    removePreTaxInvestmentTotal(code);

    $('#tblPreTaxInvestmentTotals tbody').append(
        '<tr class="pretaxinvestment-total" data-code="' + code + '">'
            + '<td>'
                + '<span>' + name + '</span>'
            + '</td>'
            + '<td>'
                + '<span>$' + value.formatMoney(2) + '</span>'
            + '</td>'
        + '</tr>');
}

function removePreTaxInvestmentTotal(code) {
    $('.pretaxinvestment-total[data-code="' + code + '"]').remove();
}

// Pre-Tax Investment Contribution Category Functions
function addPreTaxInvestmentContributionCategory(name, value, isAutoPopulate) {
    var code = convertNameToCode(name);
    var originalName = name;
    removePreTaxInvestmentContributionCategory(code);

    $('#tblPreTaxInvestmentContributionCategories tbody').append(
        '<tr class="pretaxinvestmentcontribution-category" data-code="' + code + '">'
            + '<td>'
                + '<span class="category-name">' + name + '</span>'
            + '</td>'
            + '<td>'
                + '<input type="text" class="form-control currency category-amount" placeholder="' + name + '" value="' + value + '" />'
            + '</td>'
            + '<td>'
                + '<button type="button" class="btn btn-danger btn-sm" onclick="removePreTaxInvestmentContributionCategory(\'' + code + '\', true);"><span data-feather="x-circle"></span></button>'
            + '</td>'
        + '</tr>');
            
    initializeChangeListeners();

    if (isAutoPopulate) {
        addPreTaxInvestmentCategory(originalName, 0, 0);
    }

    calculateTotals();
}

function removePreTaxInvestmentContributionCategory(code, useConfirmation) {
    if (!useConfirmation || confirm('Are you sure you want to remove this line item?')) {
        $('.pretaxinvestmentcontribution-category[data-code="' + code + '"]').remove();

        if (useConfirmation) {
            $('.pretaxinvestment-category[data-code="' + code + '"]').remove();
        }

        calculateTotals();
    }
}

function showAddPreTaxInvestmentContributionCategory() {
    $('#dialog-form .modal-title').html('Add Pre-Tax Contribution Category');
    $('#dialog-form #hdnCategoryType').val('PreTaxInvestmentContribution');
    $('#dialog-form').modal('show');
}

function addPreTaxInvestmentContributionTotal(name, value) {
    var code = convertNameToCode(name);
    removePreTaxInvestmentContributionTotal(code);

    $('#tblPreTaxInvestmentContributionTotals tbody').append(
        '<tr class="pretaxinvestmentcontribution-total" data-code="' + code + '">'
            + '<td>'
                + '<span>' + name + '</span>'
            + '</td>'
            + '<td>'
                + '<span>$' + value.formatMoney(2) + '</span>'
            + '</td>'
        + '</tr>');
}

function removePreTaxInvestmentContributionTotal(code) {
    $('.pretaxinvestmentcontribution-total[data-code="' + code + '"]').remove();
}

// Total Savings
function addSavingsTotal(name, value, isPercent) {
    var code = convertNameToCode(name);
    removeSavingsTotal(code);

    var currencySymbol = '$';
    var percentSymbol = '';

    if (isPercent) {
        percentSymbol = '%';
        currencySymbol = '';
    }

    $('#tblSavingsTotals tbody').append(
        '<tr class="savings-total" data-code="' + code + '">'
            + '<td>'
                + '<span>' + name + '</span>'
            + '</td>'
            + '<td>'
                + '<span>' + currencySymbol + value.formatMoney(2) + percentSymbol + '</span>'
            + '</td>'
        + '</tr>');
}

function removeSavingsTotal(code) {
    $('.savings-total[data-code="' + code + '"]').remove();
}

function addCategoryType() {
    var categoryName = $('#dialog-form #txtCategoryName').val();

    if (categoryName) {            
        var categoryAmount = parseFloat(cleanValue($('#dialog-form #txtCategoryAmount').val()));
        var categoryInterestRate = parseFloat(cleanValue($('#dialog-form #txtCategoryInterestRate').val()));
        var categoryType = $('#dialog-form #hdnCategoryType').val();

        if (categoryType == 'Expense') {
            addExpenseCategory(categoryName, categoryAmount);
        }
        else if (categoryType == 'Income') {
            addIncomeCategory(categoryName, categoryAmount);
        }
        else if (categoryType == 'Investment') {                
            addInvestmentCategory(categoryName, categoryInterestRate, categoryAmount);
        }
        else if (categoryType == 'PreTaxInvestment') {
            addPreTaxInvestmentCategory(categoryName, categoryInterestRate, categoryAmount, true);
        }
        else if (categoryType == 'PreTaxInvestmentContribution') {
            addPreTaxInvestmentContributionCategory(categoryName, categoryAmount, true);
        }

        $('#dialog-form').modal('hide');
        
        calculateTotals();
        setTimeout(initializeCurrency, 1);
    }
    else {
        $('#dialog-form #txtCategoryNameError').show();
    }

    return false;
}    

function calculateTotals() {
    if (initialized) {
        // Monthly Expense Categories
        var totalExpenses = 0;
        $('.expense-category').each(function () {
            totalExpenses += parseFloat(cleanValue($(this).find('.category-amount').first().val()));
        });

        addExpenseTotal('Monthly Expenses Total', totalExpenses);
        addExpenseTotal('1 Year Expenses', totalExpenses * 12);
        addExpenseTotal('5 Year Expenses', totalExpenses * 12 * 5);
        addExpenseTotal('10 Year Expenses', totalExpenses * 12 * 10);
        addExpenseTotal('15 Year Expenses', totalExpenses * 12 * 15);
        addExpenseTotal('20 Year Expenses', totalExpenses * 12 * 20);

        // Monthly Income Categories
        var totalIncome = 0;
        $('.income-category').each(function () {
            totalIncome += parseFloat(cleanValue($(this).find('.category-amount').first().val()));
        });

        addIncomeTotal('Monthly Income Total', totalIncome);
        addIncomeTotal('1 Year Income', totalIncome * 12);
        addIncomeTotal('5 Year Income', totalIncome * 12 * 5);
        addIncomeTotal('10 Year Income', totalIncome * 12 * 10);
        addIncomeTotal('15 Year Income', totalIncome * 12 * 15);
        addIncomeTotal('20 Year Income', totalIncome * 12 * 20);

        // Investment Categories
        var totalInvestment = 0;
        var investmentList = [];
        $('.investment-category').each(function () {
            var investmentAmount = parseFloat(cleanValue($(this).find('.category-amount').first().val()));
            var interestRateAmount = parseFloat(cleanValue($(this).find('.category-interestrate').first().val()));
            var interestRate = (interestRateAmount / 100);

            investmentList.push({ Amount: investmentAmount, InterestRate: interestRate });
            totalInvestment += investmentAmount;
        });

        addInvestmentTotal('Initial Investment Total', totalInvestment);

        var yearlySavings = (totalIncome - totalExpenses) * 12;

        var yearSavingsWithInterestEarnedPerDayList = [];
        var yearSavingsWithInterestList = [];
        for (var year = 1; year <= 20; year++) {
            var lastTotalInvestment = totalInvestment;
            var totalInvestmentPlusInterest = totalInvestment;

            $.each(investmentList, function () {
                totalInvestmentPlusInterest += (this.Amount * this.InterestRate);
                this.Amount += (this.Amount * this.InterestRate) + (yearlySavings / investmentList.length);
            });

            var totalInvestmentWithSavings = totalInvestmentPlusInterest + yearlySavings;
            var totalInvestmentNoSavings = totalInvestmentPlusInterest;

            totalInvestment = totalInvestmentWithSavings;

            if (year == 1 || year == 5 || year == 10 || year == 15 || year == 20) {
                addInvestmentTotal(year + ' Year - Earned Per Day', (totalInvestmentNoSavings - lastTotalInvestment) / 365);
                addInvestmentTotal(year + ' Year Investment Total', totalInvestmentWithSavings);
            }

            yearSavingsWithInterestEarnedPerDayList.push((totalInvestmentNoSavings - lastTotalInvestment) / 365);
            yearSavingsWithInterestList.push(totalInvestmentWithSavings);
        }            

        // Pre-Tax Investment Categories
        var totalPreTaxInvestment = 0;
        var preTaxInvestmentList = [];
        $('.pretaxinvestment-category').each(function () {
            var preTaxInvestmentAmount = parseFloat(cleanValue($(this).find('.category-amount').first().val()));
            var interestRateAmount = parseFloat(cleanValue($(this).find('.category-interestrate').first().val()));
            var interestRate = (interestRateAmount / 100);

            preTaxInvestmentList.push({ Amount: preTaxInvestmentAmount, InterestRate: interestRate });
            totalPreTaxInvestment += preTaxInvestmentAmount;
        });

        addPreTaxInvestmentTotal('Initial Pre-Tax Investment Total', totalPreTaxInvestment);
        
        var yearPreTaxInvestmentWithInterestEarnedPerDayList = [];
        var yearPreTaxInvestmentWithInterestList = [];
        for (var year = 1; year <= 20; year++) {
            var lastTotalPreTaxInvestment = totalPreTaxInvestment;
            var totalPreTaxInvestmentPlusInterest = totalPreTaxInvestment;

            var index = 0;
            var yearlyPreTaxContributions = 0;
            $.each(preTaxInvestmentList, function () {
                totalPreTaxInvestmentPlusInterest += (this.Amount * this.InterestRate);
                var yearlyPreTaxContribution = getPreTaxInvestmentContributionPerYear(index);
                this.Amount += (this.Amount * this.InterestRate) + yearlyPreTaxContribution;
                yearlyPreTaxContributions += yearlyPreTaxContribution;
                index++;
            });

            var totalPreTaxInvestmentWithContributions = totalPreTaxInvestmentPlusInterest + yearlyPreTaxContributions;
            var totalPreTaxInvestmentNoContributions = totalPreTaxInvestmentPlusInterest;

            totalPreTaxInvestment = totalPreTaxInvestmentWithContributions;

            if (year == 1 || year == 5 || year == 10 || year == 15 || year == 20) {
                addPreTaxInvestmentTotal(year + ' Year - Earned Per Day', (totalPreTaxInvestmentNoContributions - lastTotalPreTaxInvestment) / 365);
                addPreTaxInvestmentTotal(year + ' Year Pre-Tax Investment Total', totalPreTaxInvestmentWithContributions);
            }

            yearPreTaxInvestmentWithInterestEarnedPerDayList.push((totalPreTaxInvestmentNoContributions - lastTotalPreTaxInvestment) / 365);
            yearPreTaxInvestmentWithInterestList.push(totalPreTaxInvestmentWithContributions);
        }

        // Pre-Tax Investment Contribution Categories
        var totalPreTaxInvestmentContribution = 0;
        $('.pretaxinvestmentcontribution-category').each(function () {
            totalPreTaxInvestmentContribution += parseFloat(cleanValue($(this).find('.category-amount').first().val()));
        });

        addPreTaxInvestmentContributionTotal('Monthly Pre-Tax Total', totalPreTaxInvestmentContribution);
        addPreTaxInvestmentContributionTotal('Yearly Pre-Tax (No Interest)', totalPreTaxInvestmentContribution * 12);
        addPreTaxInvestmentContributionTotal('5 Year Pre-Tax (No Interest)', totalPreTaxInvestmentContribution * 12 * 5);
        addPreTaxInvestmentContributionTotal('10 Year Pre-Tax (No Interest)', totalPreTaxInvestmentContribution * 12 * 10);
        addPreTaxInvestmentContributionTotal('15 Year Pre-Tax (No Interest)', totalPreTaxInvestmentContribution * 12 * 15);
        addPreTaxInvestmentContributionTotal('20 Year Pre-Tax (No Interest)', totalPreTaxInvestmentContribution * 12 * 20);

        // Total Savings
        addSavingsTotal('Hourly Savings', (yearlySavings / 365) / 24);
        addSavingsTotal('Daily Savings', yearlySavings / 365);
        addSavingsTotal('Monthly Savings', totalIncome - totalExpenses);
        addSavingsTotal('1 Year Savings', yearlySavings);
        addSavingsTotal('5 Year Savings', yearlySavings * 5);
        addSavingsTotal('10 Year Savings', yearlySavings * 10);
        addSavingsTotal('15 Year Savings', yearlySavings * 15);
        addSavingsTotal('20 Year Savings', yearlySavings * 20);
        addSavingsTotal('% Savings from Monthly Income', ((totalIncome - totalExpenses) / totalIncome) * 100, true);

        addSavingsTotal('Monthly Savings w/ Pre-Tax (No Interest)', (totalIncome - totalExpenses) + totalPreTaxInvestmentContribution);
        addSavingsTotal('1 Year Savings w/ Pre-Tax (No Interest)', ((totalIncome - totalExpenses) + totalPreTaxInvestmentContribution) * 12);
        addSavingsTotal('5 Year Savings w/ Pre-Tax (No Interest)', ((totalIncome - totalExpenses) + totalPreTaxInvestmentContribution) * 12 * 5);
        addSavingsTotal('10 Year Savings w/ Pre-Tax (No Interest)', ((totalIncome - totalExpenses) + totalPreTaxInvestmentContribution) * 12 * 10);
        addSavingsTotal('15 Year Savings w/ Pre-Tax (No Interest)', ((totalIncome - totalExpenses) + totalPreTaxInvestmentContribution) * 12 * 15);
        addSavingsTotal('20 Year Savings w/ Pre-Tax (No Interest)', ((totalIncome - totalExpenses) + totalPreTaxInvestmentContribution) * 12 * 20);

        addSavingsTotal('1 Year Savings w/ Pre-Tax w/ Interest - Earned Per Day', yearSavingsWithInterestEarnedPerDayList[1 - 1] + yearPreTaxInvestmentWithInterestEarnedPerDayList[1 - 1]);
        addSavingsTotal('1 Year Savings w/ Pre-Tax w/ Interest', yearSavingsWithInterestList[1 - 1] + yearPreTaxInvestmentWithInterestList[1 - 1]);

        addSavingsTotal('5 Year Savings w/ Pre-Tax w/ Interest - Earned Per Day', yearSavingsWithInterestEarnedPerDayList[5 - 1] + yearPreTaxInvestmentWithInterestEarnedPerDayList[5 - 1]);
        addSavingsTotal('5 Year Savings w/ Pre-Tax w/ Interest', yearSavingsWithInterestList[5 - 1] + yearPreTaxInvestmentWithInterestList[5 - 1]);

        addSavingsTotal('10 Year Savings w/ Pre-Tax w/ Interest - Earned Per Day', yearSavingsWithInterestEarnedPerDayList[10 - 1] + yearPreTaxInvestmentWithInterestEarnedPerDayList[10 - 1]);
        addSavingsTotal('10 Year Savings w/ Pre-Tax w/ Interest', yearSavingsWithInterestList[10 - 1] + yearPreTaxInvestmentWithInterestList[10 - 1]);

        addSavingsTotal('15 Year Savings w/ Pre-Tax w/ Interest - Earned Per Day', yearSavingsWithInterestEarnedPerDayList[15 - 1] + yearPreTaxInvestmentWithInterestEarnedPerDayList[15 - 1]);
        addSavingsTotal('15 Year Savings w/ Pre-Tax w/ Interest', yearSavingsWithInterestList[15 - 1] + yearPreTaxInvestmentWithInterestList[15 - 1]);

        addSavingsTotal('20 Year Savings w/ Pre-Tax w/ Interest - Earned Per Day', yearSavingsWithInterestEarnedPerDayList[20 - 1] + yearPreTaxInvestmentWithInterestEarnedPerDayList[20 - 1]);
        addSavingsTotal('20 Year Savings w/ Pre-Tax w/ Interest', yearSavingsWithInterestList[20 - 1] + yearPreTaxInvestmentWithInterestList[20 - 1]);

        saveInputValues();
    }
}

function getPreTaxInvestmentContributionPerYear(index) {
    var indexVal = 0;
    var returnVal = 0;
    $('.pretaxinvestmentcontribution-category').each(function () {
        if (indexVal == index) {
            returnVal = parseFloat(cleanValue($(this).find('.category-amount').first().val()));
        }

        indexVal++;
    });

    return returnVal * 12;
}

function saveInputValues() {
    var budgetValues = [];

    $('.expense-category').each(function () {
        var obj = new Object();
        obj.name = $(this).find('.category-name').first().html();
        obj.interestRate = null;
        obj.amount = parseFloat(cleanValue($(this).find('.category-amount').first().val()));
        obj.type = 'Expense';

        budgetValues.push(obj);
    });

    $('.income-category').each(function () {
        var obj = new Object();
        obj.name = $(this).find('.category-name').first().html();
        obj.interestRate = null;
        obj.amount = parseFloat(cleanValue($(this).find('.category-amount').first().val()));
        obj.type = 'Income';

        budgetValues.push(obj);
    });

    $('.investment-category').each(function () {
        var obj = new Object();
        obj.name = $(this).find('.category-name').first().html();
        obj.interestRate = parseFloat(cleanValue($(this).find('.category-interestrate').first().val()));
        obj.amount = parseFloat(cleanValue($(this).find('.category-amount').first().val()));
        obj.type = 'Investment';

        budgetValues.push(obj);
    });

    $('.pretaxinvestment-category').each(function () {
        var obj = new Object();
        obj.name = $(this).find('.category-name').first().html();
        obj.interestRate = parseFloat(cleanValue($(this).find('.category-interestrate').first().val()));
        obj.amount = parseFloat(cleanValue($(this).find('.category-amount').first().val()));
        obj.type = 'PreTaxInvestment';

        budgetValues.push(obj);
    });

    $('.pretaxinvestmentcontribution-category').each(function () {
        var obj = new Object();
        obj.name = $(this).find('.category-name').first().html();
        obj.interestRate = null;
        obj.amount = parseFloat(cleanValue($(this).find('.category-amount').first().val()));
        obj.type = 'PreTaxInvestmentContribution';

        budgetValues.push(obj);
    });

    $.each(budgetValues, function () {
        this.code = convertNameToCode(this.name)
    })

    localStorage.setObject('BudgetValues', budgetValues);
}

function loadInputValues(budgetValues) {
    $(budgetValues).each(function () {

        var obj = this;
        if (obj.type == 'Expense') {
            addExpenseCategory(obj.name, obj.amount);
        }
        else if (obj.type == 'Income') {
            addIncomeCategory(obj.name, obj.amount);
        }
        else if (obj.type == 'Investment') {
            addInvestmentCategory(obj.name, obj.interestRate, obj.amount);
        }
        else if (obj.type == 'PreTaxInvestment') {
            addPreTaxInvestmentCategory(obj.name, obj.interestRate, obj.amount);
        }
        else if (obj.type == 'PreTaxInvestmentContribution') {
            addPreTaxInvestmentContributionCategory(obj.name, obj.amount);
        }
    });
}

function initalizeDefaultBudgetValues() {
    addExpenseCategory('Rent', 1500);
    addExpenseCategory('Electricity', 75);
    addExpenseCategory('Internet', 80);
    addExpenseCategory('Phone', 100);
    addExpenseCategory('Groceries', 400);
    addExpenseCategory('Insurance', 120);
    addExpenseCategory('Clothing', 100);
    addExpenseCategory('Entertainment', 500);
    addExpenseCategory('Vacation', 2000);
    addExpenseCategory('Fuel', 100);
    addExpenseCategory('Vehicle Maintenance', 100);
    addExpenseCategory('Fitness', 50);
    addExpenseCategory('Haircut', 15);

    addIncomeCategory('Paycheck', 8000);

    addInvestmentCategory('1 Year CD', 3, 180000);
    addInvestmentCategory('Savings', 2, 15000);

    addPreTaxInvestmentCategory('401k', 6, 400000);
    addPreTaxInvestmentCategory('Traditional IRA', 5, 100000);
    addPreTaxInvestmentCategory('HSA', 2, 15000);

    addPreTaxInvestmentContributionCategory('401k', 2000);
    addPreTaxInvestmentContributionCategory('Traditional IRA', 250);
    addPreTaxInvestmentContributionCategory('HSA', 200);
}

function convertNameToCode(name) {
    return name.replace(/\\/g, '')
        .replace(/\//g, '')
        .replace(/,/g, '')
        .replace(/\-/g, '')
        .replace(/\(/g, '')
        .replace(/\)/g, '')
        .replace(/\s/g, '');
}

function initializeChangeListeners() {
    if (initialized) {
        $('.currency').off('change');
        $('.currency').on('change', function () {
            calculateTotals();
        });

        $('.percentage').off('change');
        $('.percentage').on('change', function () {
            calculateTotals();
        });
    }
}

function initializeCurrency() {
    $('.currency').autoNumeric('destroy');
    $('.currency').autoNumeric('init', { aSign: '$' });

    $('.percentage').autoNumeric('destroy');
    $('.percentage').autoNumeric('init', { aSign: '%', pSign: 's' });

    initializeIcons();
}

function clearBudgetValues() {
    if (confirm('Are you sure you want to clear the budget values and start over?')) {
        $('.expense-category').remove();
        $('.income-category').remove();
        $('.investment-category').remove();
        $('.pretaxinvestment-category').remove();
        $('.pretaxinvestmentcontribution-category').remove();

        calculateTotals();
    }
}

function resetBudgetValuesToExampleDefaults() {
    if (confirm('Are you sure you want to reset the budget values to the example defaults?')) {
        localStorage.removeItem('BudgetValues');
        location.reload();
    }
}

var initialized = false;

function initialize() {
    $('#dialog-form').modal({
        keyboard: false,
        backdrop: 'static',
        show: false
    });

    $('#dialog-form').on('shown.bs.modal', function (e) {
        $('#txtCategoryName').focus();
    });

    $('#dialog-form').on('hide.bs.modal', function (e) {
        $('#dialog-form #txtCategoryName').val('');
        $('#dialog-form #txtCategoryNameError').hide();
        $('#dialog-form #txtCategoryInterestRate').val('');
        $('#dialog-form #pnlCategoryInterestRate').hide();
        $('#dialog-form #txtCategoryAmount').val('');
    });

    $('#btnClearBudgetValues').click(function () {
        clearBudgetValues();
    });

    $('#btnResetBudgetValuesToExampleDefaults').click(function () {
        resetBudgetValuesToExampleDefaults();
    });

    var budgetValues = localStorage.getObject('BudgetValues');

    if (budgetValues == null || budgetValues.length == 0) {
        initalizeDefaultBudgetValues();
    }
    else {
        loadInputValues(budgetValues);
    }

    initialized = true;
    initializeChangeListeners();
    initializeCurrency();        

    calculateTotals();
}

    $(function () {    
    initialize();
    $('#loader').hide();
    $('#app').show();
    });