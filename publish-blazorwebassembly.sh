#!/bin/bash

chmod u+x ./config-blazorwebassembly.sh
./config-blazorwebassembly.sh

dotnet publish -c Release

rm -rf public/_framework/
rm -rf public/css/
rm -rf public/scripts/
rm -rf public/vuejs/
rm -rf public/jquery/
cp -r bin/Release/netstandard2.1/publish/wwwroot/_framework public/_framework
cp -r bin/Release/netstandard2.1/publish/wwwroot/css public/css
cp -r bin/Release/netstandard2.1/publish/wwwroot/scripts public/scripts
cp -r bin/Release/netstandard2.1/publish/wwwroot/vuejs public/vuejs
cp -r bin/Release/netstandard2.1/publish/wwwroot/jquery public/jquery